const express = require('express');
const router = express.Router();
const { graphqlHTTP } = require('express-graphql');
const jwt = require('jsonwebtoken');
const schema = require('../grapql/eschema');
const root= require('../grapql/root');
const {authenticateToken} = require('../middlewares/authenticateToken')
require('dotenv').config()

router.get('/token', (req, res) => {
    const payload = {
            iss: "wincify.com",
            sub: "user_id",
            tenantId: "9cbe545399216e916a763c60a25c406c",
            applicationId: "020fc762b2d63e1acc9b00f9fc452418",
            groups: [
            "list of groups"
            ],
            roles: [
            "administrador",
            "comentarista",
            "escritor"
            ],
            
    }
    const token = jwt.sign(payload, process.env.KEY, {
        expiresIn: 1440
    });
    res.send({
        token: "Bearer "+token
    });
})
router.get('/token/bad', (req, res) => {
    const payload = {
            iss: "wincify.com",
            sub: "user_id",
            tenantId: "9cbe545399216e916a763c60a25c406c",
            applicationId: "020fc762b2d63e1acc9b00f9fc452418",
            groups: [
            "list of groups"
            ],
            roles: [
            "comentarista",
            "escritor"
            ],
            
    }
    const token = jwt.sign(payload, process.env.KEY, {
        expiresIn: 1440
    });
    res.send({
        token: "Bearer "+token
    });
})
router.use(authenticateToken);
router.post('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: false,
}));


module.exports = router;
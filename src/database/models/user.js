const { Model } = require('objection');
require('dotenv').config()
var config = require('../knexfile.js')[process.env.NODE_ENV || 'development']
const knex=require('knex')(config)

Model.knex(knex)

class User extends Model {
  static get tableName() {
    return 'users';
  }
}

module.exports = User;
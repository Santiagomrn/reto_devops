
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        { name: 'Antonia',phone:'990 922 9340', email:'xd@gmail.com'},
        { name: 'marcos', phone:'988 123 9092',email:'marcos@hotmail.com'},
        { name: 'miguel', phone:'123 134 3909',email:'mike@gmail.com'}
      ]);
    });
};

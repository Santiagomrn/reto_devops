const jwt = require('jsonwebtoken');
require('dotenv').config()
module.exports = {
authenticateToken: function (req, res, next) {
    // Gather the jwt access token from the request header
    let authHeader = req.headers['authorization'] 
    if (authHeader == null) return res.sendStatus(401) // if there isn't any token
    var [barer,token] = authHeader.split(' ');
    
    try {
        var decoded = jwt.verify(token,process.env.KEY);
        if(decoded.roles.includes('administrador')){
            next()
        }else{
            throw new Error
        }
      } catch(err) {
        console.log("token invalido")
        res.sendStatus(401)
      }
  }
}
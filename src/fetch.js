const fetch = require('node-fetch');
var query = `
query User($id:String!) {
    get_user(id:$id) {
      name 
      email 
      phone
    }   
}`;
fetch('http://localhost:3000/graphql', {
  method: 'POST',
  headers: { 'Content-Type': 'application/json',
Authorization:"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ3aW5jaWZ5LmNvbSIsInN1YiI6InVzZXJfaWQiLCJ0ZW5hbnRJZCI6IjljYmU1NDUzOTkyMTZlOTE2YTc2M2M2MGEyNWM0MDZjIiwiYXBwbGljYXRpb25JZCI6IjAyMGZjNzYyYjJkNjNlMWFjYzliMDBmOWZjNDUyNDE4IiwiZ3JvdXBzIjpbImxpc3Qgb2YgZ3JvdXBzIl0sInJvbGVzIjpbImFkbWluaXN0cmFkb3IiLCJjb21lbnRhcmlzdGEiLCJlc2NyaXRvciJdLCJpYXQiOjE2MDI5MTU5ODcsImV4cCI6MTYwMjkxNzQyN30.jTdKjE3a-WUabom31S7HyB2MHbOK54taA4MW3SAnjgc" },
  body: JSON.stringify
  ({ query,
    variables:{id:"1"}  
  }),
})
  .then(res => res.json())
  .then(res => console.log(res))
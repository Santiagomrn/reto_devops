const { buildSchema } = require('graphql');
const schema = buildSchema(`
type Query {
    get_user( id : String! ) : User
    }
    type Mutation {
    create_user( user : UserInput ) : User
    edit_user( id : String! , user : UserInput ) : User
    delete_user( id : String! ) : String
    }
    type User {
    name : String!
    phone : String!
    email : String!
    }
    input UserInput {
    name : String!
    phone : String!
    email : String
}
`);
module.exports=schema;
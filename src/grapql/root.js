const User = require('../database/models/user')
const root = {
    get_user: async ({ id }) => {
        const user_data = await User.query().findById(id)
        return user_data
    },
    create_user: async ({ user }) => {
        console.log(user)
        const user_data = await User.query().insert(user);
        return user_data
    },
    edit_user: async ({ id, user }) => {
        const user_data = await User.query()
            .findById(id)
            .patch(user);
        const update_user_data = await User.query().findById(id)
        return update_user_data
    }
    ,
    delete_user: async ({ id }) => {
        const user_data = await User.query().deleteById(id);
        console.log(user_data)
        return id
    }

};
module.exports=root;